package com.soyuan;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;
import com.soyuan.model.BusinessDTO;
import com.soyuan.model.OfficerDTO;
import com.soyuan.util.MongoUtil;
import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.mongodb.client.model.Filters.*;

/**
 * Created by twx on 2017/10/23.
 */
public class MongoTest {
    public Gson gson = new Gson();

//    @Before
//    public void before() {
//        MongoClient client = new MongoClient("localhost", 27017);
//        MongoDatabase testDB = client.getDatabase("test");
//    }
    @Test
    public void testMain(){
        MongoClient client = new MongoClient("localhost", 27017);
        MongoDatabase testDB = client.getDatabase("test");
        System.out.println("success");
        MongoCollection<Document> restaurants = testDB.getCollection("restaurants");


        BusinessDTO bto = new BusinessDTO("KLF23HU","OIU112","MKL42H","RTE213","Google", LocalDateTime.now().toString());
        bto.setExamineOfficer(new OfficerDTO("HDHG001","李蛤奤"));
        bto.setExamineOfficer(new OfficerDTO("HDHG002","李大锤"));
        bto.setGood("手机");

        String btoJson =gson.toJson(bto);
        Document document = Document.parse(btoJson);

        restaurants.insertOne(document);
//        Document first = restaurants.find().first();
//        System.out.println(first.toJson());
        Document first = restaurants.find(eq("entryNo", "OIU112")).first();
        System.out.println(first.toJson());
//        restaurants.findan
    }

    @Test
    public void testFindSize(){
        MongoCollection<Document> actionClt = MongoUtil.getCollection("actionData");
//        BasicDBObject obj = new BasicDBObject("_id",new ObjectId("59ef046c1b4d2a20ac91bdbc"));
        List<Document> actionList = actionClt.find(new Document("_id",new ObjectId("59ef046c1b4d2a20ac91bdbc"))).into(new ArrayList<Document>());
        System.out.println(actionList.size());
        int size = actionList.size();
        Random random = new Random();
        Document document = actionList.get(random.nextInt(size));
        String status = document.getString("status");
        System.out.println(status);
        Object id = document.get("_id");
        System.out.println(id);
    }

    @Test
    public void testCleanAllData(){
        MongoCollection<Document> actionClt = MongoUtil.getCollection("exData");
        actionClt.deleteMany(new Document());
    }

    @Test
    public void testLocalDate() {
        MongoCollection<Document> actionClt = MongoUtil.getCollection("actionData");
        String str = LocalDate.now().toString();
        System.out.println(str);
        List<Document> list = actionClt.find(
                regex("data.actionTime","2017-10-30")).into(new ArrayList<Document>());
        System.out.println(list.size());

    }
}
