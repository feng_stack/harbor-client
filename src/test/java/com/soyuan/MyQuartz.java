package com.soyuan;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

/**
 * Created by twx on 2017/10/25.
 */
public class MyQuartz {
    public static void main(String[] args) {
        JobDetail job = JobBuilder.newJob(HelloJob.class)
                .withIdentity("twxJobName", "group1").build();

//        JobDetail job1 = JobBuilder.newJob(HelloJob.class)
//                .withIdentity("twxJob1", "group1").build();



        Trigger trigger1 = TriggerBuilder.newTrigger()
                .withIdentity("trigger1","group1")
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                .withRepeatCount(0))
                .build();

        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("trigger", "group1")
                .withSchedule(CronScheduleBuilder.cronSchedule("0 02 00 ? * MON-FRI"))
                .forJob(JobKey.jobKey("twxJobName","group1"))
                .build();

        try {
            Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.start();
            scheduler.scheduleJob(job,trigger1);
            scheduler.scheduleJob(trigger);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
}


