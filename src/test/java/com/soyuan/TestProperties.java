package com.soyuan;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created by twx on 2017/10/26.
 */
public class TestProperties {

    @Test
    public void test() throws IOException {
        Properties properties = new Properties();

//        InputStream is = new FileInputStream("url.properties");
        InputStream is = TestProperties.class.getClassLoader().getResourceAsStream("url.properties");

        properties.load(is);


        String url = (String) properties.get("etl.pushExUrl");
        System.out.println(url);

    }

    @Test
    public void shuttle() {
     List<String> list = new ArrayList<>(Arrays.asList("1","2","3","4","5","6","7"));
        Collections.shuffle(list);

        for (String str : list) {
            System.out.println(str);
        }

    }
}
