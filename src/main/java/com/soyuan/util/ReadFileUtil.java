package com.soyuan.util;

import com.soyuan.model.NodeField;
import com.soyuan.model.OfficerDTO;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by twx on 2017/10/25.
 */
public class ReadFileUtil {


    public static  List<String> read(String path) throws IOException {
        List<String> list = new ArrayList<>();

        InputStream is = ReadFileUtil.class.getClassLoader().getResourceAsStream(path);

        BufferedReader br = new BufferedReader(new InputStreamReader(is,"utf-8"));
        String line;
        while ((line = br.readLine()) != null) {
            if (!list.contains(line))
                list.add(line);
        }
        is.close();
        br.close();
        return list;
    }

    public static List<NodeField> getExField(String path) throws IOException {
        List<NodeField> list = new ArrayList<>();

        InputStream is = ReadFileUtil.class.getClassLoader().getResourceAsStream(path);

        BufferedReader br = new BufferedReader(new InputStreamReader(is,"gbk"));
        String line;
        while ((line = br.readLine()) != null) {
            String[] arr = line.split(",");
            NodeField nodeField = new NodeField(arr[0], arr[1], arr[2]);
            list.add(nodeField);
        }
        is.close();
        br.close();
        return list;
    }

    public static String getProperty(String key) throws IOException{
        try(InputStream is = ReadFileUtil.class.getClassLoader().getResourceAsStream("appconfig.properties")){
            Properties properties = new Properties();
            properties.load(is);
            return properties.getProperty(key);
        }
    }



    public static void main(String[] args) throws IOException {
//        ReadFileUtil util = new ReadFileUtil();
//        List<String> read = read("company.txt");
//        System.out.println(read.size());
//        System.out.println(read.get(0));

//        List<NodeField> exField = getExField("examField.csv");
//        System.out.println(exField.size());
//        String name = exField.get(0).getExFieldName();
//        System.out.println(name);

//        String size = getProperty("exLocationSize");
//        System.out.println(size);


//        List<OfficerDTO> officers = OfficerDTO.getOfficers("officers.txt");
//        System.out.println(officers.get(0).getId()+" "+officers.get(0).getName());
    }
}
