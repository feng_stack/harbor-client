package com.soyuan.util;

import java.util.Random;

/**
 * Created by twx on 2017/10/24.
 */
public class RandomTime {

    private static Random random = new Random();

    public static int luoxiang(){
        int arr[] = {5,10,15};
        int i = random.nextInt(3);
        return arr[i];
    }

    public static int beginTao(){
        int arr[] = {15,20,25,30};
        int i = random.nextInt(4);
        return arr[i];
    }

    public static int endTao(){
        int arr[] = {15,20,25};
        int i = random.nextInt(3);
        return arr[i];
    }

    public static int completePackage(){
        int arr[] = {20,25,30,35,40};
        int i = random.nextInt(5);
        return arr[i];
    }

    public static int exit(){
        int arr[] = {10,15,20,25,30,35,40,45,50,55,60};
        int i = random.nextInt(11);
        return arr[i];
    }

    public static void main(String[] args) {
        for(int k=0;k<20;k++) {
            System.out.println(exit());
        }
    }
}
