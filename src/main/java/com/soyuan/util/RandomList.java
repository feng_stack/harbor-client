package com.soyuan.util;

import com.soyuan.model.OfficerDTO;

import java.util.List;
import java.util.Random;

/**
 * Created by twx on 2017/10/25.
 */
public class RandomList {
    private static Random random= new Random();

    public static String getRandom(List<String> list) {
        int index = random.nextInt(list.size());
        return list.get(index);
    }

    public static OfficerDTO getRandomOfficer(List<OfficerDTO> list) {
        int index = random.nextInt(list.size());
        return list.get(index);
    }
}
