package com.soyuan.util;

import java.util.Random;

/**
 * Created by twx on 2017/10/24.
 */
public class RandomContainer {

    public static String[] arr = {"A","B","C","D","E","F","G","H","I"
            ,"J","K","L","M","N","O","P","Q","R","S","T"
            ,"U","V","W","X","Y","Z",
            "1","2","3","4","5","6","7","8","9","0","-"};

    public static String[] letters = {"A","B","C","D","E","F","G","H","I"
            ,"J","K","L","M","N","O","P","Q","R","S","T"
            ,"U","V","W","X","Y","Z"};
    public static String[] numbers={"1","2","3","4","5","6","7","8","9","0"};

    private  static  Random random = new Random();

    public static String generator() {
        int max=16;
        int min=7;
        int length = random.nextInt(max)%(max-min+1) + min;
        String str="";
        for (int i=0;i<length;i++) {
            int s = random.nextInt(37);
            String temp = arr[s];
            if (i == 0 || i == length - 1) {
                if (temp.equals("-")) {
                    continue;
                }
            }
            str+=temp;
        }
        return str;
    }

    public static int generatorInt(int min, int max) {
        return  random.nextInt(max)%(max-min+1) + min;
    }

    public static StringBuilder generatorLetters(int length) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int randomInt = random.nextInt(letters.length);
            sb.append(letters[randomInt]);
        }
        return sb;
    }

    public static String generatorNum(int length, StringBuilder sb) {
        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(numbers.length);
            sb.append(numbers[randomIndex]);
        }
        return sb.toString();
    }

    public static String generatorNum(int length) {
        return generatorNum(length,new StringBuilder());
    }

    public static String generatorContainerNo() {
        return  generatorNum(10, generatorLetters(4));
    }

    public static String generatorExamRecord(){
        return  generatorNum(4)+"2017"+generatorNum(10);
    }

    public static void main(String[] args) {
        String str = generatorContainerNo();
        System.out.println(str);
    }
}
