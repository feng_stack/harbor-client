package com.soyuan;

import com.google.gson.Gson;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.soyuan.model.*;
import com.soyuan.util.MongoUtil;
import com.soyuan.util.RandomContainer;
import com.soyuan.util.RandomList;
import com.soyuan.util.ReadFileUtil;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.set;

/**
 * Created by twx on 2017/10/25.
 */
public class Main {
    private static Gson gson = new Gson();
    private static List<String> containerList,companyList,billNoList,goodsList;

    public void init() throws IOException {
        containerList = ReadFileUtil.read("container.txt");
        companyList = ReadFileUtil.read("company.txt");
        billNoList = ReadFileUtil.read("billNo.txt");
        goodsList = ReadFileUtil.read("goods.txt");
    }
//    public static void main(String[] args) throws IOException {
//        Main main = new Main();
//        main.init();
//
//        MongoCollection<Document> exDataClt = MongoUtil.getCollection("exData");
//        MongoCollection<Document> actionClt = MongoUtil.getCollection("actionData");
//
//        while (true) {
//
//            LocalTime nowtime = LocalTime.now();
//
//            //早上九点（服务启动） 构造200个业务数据（箱子），推到 ETL,同时存入Mongo
//            if(nowtime.getHour()==9&&nowtime.getMinute()==0){
//                System.out.println("***********");
//                for (int i = 0; i < 200; i++) {
//                    BusinessDTO bto= main.generatorBusinessData();
//                    Document doc = Document.parse(gson.toJson(bto));
//                    exDataClt.insertOne(doc);
//
//                    //call api 发送到 ETL
//                }
//
//                List<NodeStatus> nodeStatusList = NodeStatus.getNodeStatusList();
//                List<NodeField> exFields = ReadFileUtil.getExField("examinField.csv");
//
//                //find in Mongo 找出所有集装箱号  轮询配置节点信息
//                MongoCursor<Document> iterator = exDataClt.find().iterator();
//                int counter=0;
//                while (iterator.hasNext()) {
//                    //服务开始时间 随机基准时间
//                    LocalDateTime time = LocalDateTime.now().withHour(9).withMinute(0);
//
//                    //1.集装箱号
//                    Document businessDoc = iterator.next();
//                    String containerNo = businessDoc.getString("containerNo");
//                    //2. 查验位位置
//                    NodeField nodeField = exFields.get(counter);
//                    counter++;
//
//                    NodeDTO ndto =  new NodeDTO(containerNo,nodeField.getExFieldCode(),nodeField.getExFieldName(),
//                            nodeField.getLocationCode(),nodeField.getSource());
//                    //3. 随机一个入场时间，设置入场节点，存入Mongo
//                    //4.基于入场时间，再随机一个落箱时间，设置落箱节点，存入Mongo
//                    //5. 如此重复，直到完成出场节点信息
//                    //6.至此，一个集装箱（即查验位预设完成）
//                    for (NodeStatus nodeStatus : nodeStatusList) {
//                        int plusMinus = RandomContainer.generatorInt(nodeStatus.getMin(), nodeStatus.getMax());
//                        time = time.plusMinutes(plusMinus);
//                        ndto.setTransferTime(time.toString());
//                        ndto.setStatusCode(nodeStatus.getCode());
//                        ndto.setStatus(nodeStatus.getStatus());
//                        ndto.setActionTime(time.toString());
//
//                        //存入mongo
//                        NodeMongo nodeMongo = new NodeMongo(false, ndto);
//                        Document nodeDoc = Document.parse(gson.toJson(nodeMongo));
//                        actionClt.insertOne(nodeDoc);
//                    }
//                }//end while
//            }//end if
//
//            //轮询节点列表 9~23点工作
//            while (true) {
//                //查询当前时间之前的、还未发送的节点信息
//                LocalTime now = LocalTime.now();
//                if (now.getHour() >= 23 || now.getHour()<9) {
//                    break;
//                }
//                System.out.println("break");
//                MongoCursor<Document> willDoneDocs = actionClt.find(and(
//                        lt("data.actionTime", now.toString()),
//                        eq("status", false))).iterator();
//                while (willDoneDocs.hasNext()) {
//                    Document next = willDoneDocs.next();
//                    Object obj = next.get("data");
//                    String json = gson.toJson(obj);
//                    System.out.println(json);
//
//                    //call api to ETL
//                    ObjectId id = next.getObjectId("_id");
////                System.out.println(id);
//                    actionClt.updateOne(eq("_id", id), set("status", true));
//                }
//                System.out.println("OK");
//
//                try {
//                    Thread.sleep(1000*60*5);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }//end while 轮询节点列表
//
//        }//end out while
//    }

    /**
     * 构造业务数据（集装箱数据）
     * @return
     */
//    public BusinessDTO generatorBusinessData(){
//        String container = RandomList.getRandom(containerList);
//        String entryNo = RandomContainer.generator();
//        String billNo = RandomList.getRandom(billNoList);
//        String examineRecordNo = RandomContainer.generator();
//        String good = RandomList.getRandom(goodsList);
//        String company = RandomList.getRandom(companyList);
//
//        BusinessDTO bto = new BusinessDTO(container,entryNo,billNo,examineRecordNo,company, LocalDateTime.now().toString());
//        bto.setGood(good);
//        bto.setExamineOfficer(new OfficerDTO("HDHG001","李蛤奤"));
//        bto.setExamineOfficer(new OfficerDTO("HDHG002","李大锤"));
//        return bto;
//    }

}
