package com.soyuan.model;

/**
 * Created by twx on 2017/9/19.
 */
public class NodeDTO {

    private String containerNo;
    private String examineRecordNo;//查验单号
    private String transferTime;
    private String examFieldCode;
    private String examFieldName;
    private String examLocationCode;
    private String statusCode;
    private String status;
    private String remark="***";
    private String source;
    private String actionTime;

    public NodeDTO(String containerNo,String examineRecordNo, String examFieldCode, String examFieldName, String examLocationCode,String source) {
        this.containerNo = containerNo;
        this.examineRecordNo = examineRecordNo;
        this.examFieldCode = examFieldCode;
        this.examFieldName = examFieldName;
        this.examLocationCode = examLocationCode;
        this.source = source;
    }

    public String getExamineRecordNo() {
        return examineRecordNo;
    }

    public void setExamineRecordNo(String examineRecordNo) {
        this.examineRecordNo = examineRecordNo;
    }

    public String getTransferTime() {
        return transferTime;
    }

    public void setTransferTime(String transferTime) {
        this.transferTime = transferTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getContainerNo() {
        return containerNo;
    }

    public void setContainerNo(String containerNo) {
        this.containerNo = containerNo;
    }

    public String getExamFieldCode() {
        return examFieldCode;
    }

    public void setExamFieldCode(String examFieldCode) {
        this.examFieldCode = examFieldCode;
    }

    public String getExamFieldName() {
        return examFieldName;
    }

    public void setExamFieldName(String examFieldName) {
        this.examFieldName = examFieldName;
    }

    public String getExamLocationCode() {
        return examLocationCode;
    }

    public void setExamLocationCode(String examLocationCode) {
        this.examLocationCode = examLocationCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getActionTime() {
        return actionTime;
    }

    public void setActionTime(String actionTime) {
        this.actionTime = actionTime;
    }
}
