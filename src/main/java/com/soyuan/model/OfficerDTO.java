package com.soyuan.model;

import com.soyuan.util.ReadFileUtil;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by twx on 2017/9/19.
 * 查验
 */
public class OfficerDTO {

    private String id;
    private String name;

    public OfficerDTO(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public static List<OfficerDTO> getOfficers(String filePath) throws IOException {
        List<OfficerDTO> list = new ArrayList<>();
        InputStream is = ReadFileUtil.class.getClassLoader().getResourceAsStream(filePath);

        BufferedReader br = new BufferedReader(new InputStreamReader(is,"utf-8"));
        String line;
        String officerId="HG";
        int count=1;
        while ((line = br.readLine()) != null) {
            OfficerDTO officerDTO = new OfficerDTO(officerId + count, line);
            list.add(officerDTO);
            count++;
        }
        is.close();
        br.close();
        return list;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
