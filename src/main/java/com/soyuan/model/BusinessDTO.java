package com.soyuan.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by twx on 2017/9/19.
 */
public class BusinessDTO {

    private String containerNo;//集装箱号

    private String entryNo;//报关单号

    private String billNo;//提运单号

    private String proxyName;//货运代理名

    private String tradeName;//经营单位名称

    private String ownerName;//货主单位名称

    private String agentName;//申报单位名称


    private String examineRecordNo;

    private List<String> goods = new ArrayList<>(); //商品名称

    private String examineTime;

    private String unstuffingRule;

    private String examineMode;

    private String examFieldCode;
    private String examSiteCode;

    private transient  List<String> examineModes= new ArrayList<>(Arrays.asList("彻底查验","外形查验","简易抽查","常规抽查","重点抽查"));

    private List<OfficerDTO> examineOfficers = new ArrayList<>();

    public BusinessDTO(String containerNo, String entryNo, String billNo, String examineRecordNo,String company, String examineTime) {
        this.containerNo = containerNo;
        this.entryNo = entryNo;
        this.billNo = billNo;
        this.examineRecordNo = examineRecordNo;
        this.examineTime = examineTime;
        this.proxyName = company;
        this.agentName = company;
        this.ownerName =  company;
        this.tradeName =company;
        this.unstuffingRule = (char)(Math.random() * 14 + 'A')+"";
        this.examineMode =examineModes.get((int) (Math.random() * 5));
    }

    public String getExamFieldCode() {
        return examFieldCode;
    }

    public void setExamFieldCode(String examFieldCode) {
        this.examFieldCode = examFieldCode;
    }

    public String getExamSiteCode() {
        return examSiteCode;
    }

    public void setExamSiteCode(String examSiteCode) {
        this.examSiteCode = examSiteCode;
    }

    public List<OfficerDTO> getExamineOfficers() {
        return examineOfficers;
    }

    public void setExamineOfficer(OfficerDTO examineOfficer) {
        this.examineOfficers.add(examineOfficer);
    }

    public String getContainerNo() {
        return containerNo;
    }

    public void setContainerNo(String containerNo) {
        this.containerNo = containerNo;
    }

    public String getEntryNo() {
        return entryNo;
    }

    public void setEntryNo(String entryNo) {
        this.entryNo = entryNo;
    }

    public String getExamineRecordNo() {
        return examineRecordNo;
    }

    public void setExamineRecordNo(String examineRecordNo) {
        this.examineRecordNo = examineRecordNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getProxyName() {
        return proxyName;
    }

    public void setProxyName(String proxyName) {
        this.proxyName = proxyName;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public List<String> getGoods() {
        return goods;
    }

    public void setGood(String good) {
        this.goods.add(good);
    }

    public String getExamineTime() {
        return examineTime;
    }

    public void setExamineTime(String examineTime) {
        this.examineTime = examineTime;
    }

    public String getUnstuffingRule() {
        return unstuffingRule;
    }

    public void setUnstuffingRule(String unstuffingRule) {
        this.unstuffingRule = unstuffingRule;
    }

    public String getExamineMode() {
        return examineMode;
    }

    public void setExamineMode(String examineMode) {
        this.examineMode = examineMode;
    }
}
