package com.soyuan.model;

/**
 * Created by twx on 2017/10/25.
 */
public class BusinessMongo {
    private String sendTime;
    private boolean status;
    private BusinessDTO data;

    public BusinessMongo(String sendTime, boolean status, BusinessDTO data) {
        this.sendTime = sendTime;
        this.status = status;
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }


    public BusinessDTO getData() {
        return data;
    }

    public void setData(BusinessDTO data) {
        this.data = data;
    }
}
