package com.soyuan.model;

/**
 * Created by twx on 2017/10/25.
 */
public class NodeField {
    private String exFieldCode;
    private String exFieldName;
    private String locationCode;
    private String source="港务";

    public NodeField(String exFieldCode, String exFieldName, String locationCode) {
        this.exFieldCode = exFieldCode;
        this.exFieldName = exFieldName;
        this.locationCode = locationCode;
    }

    public String getExFieldCode() {
        return exFieldCode;
    }

    public void setExFieldCode(String exFieldCode) {
        this.exFieldCode = exFieldCode;
    }

    public String getExFieldName() {
        return exFieldName;
    }

    public void setExFieldName(String exFieldName) {
        this.exFieldName = exFieldName;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
