package com.soyuan.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by twx on 2017/10/25.
 */
public class NodeStatus {
    private String code;
    private String status;
    private int min;
    private int max;


    public NodeStatus(String code, String status,int mix,int max) {
        this.code = code;
        this.status = status;
        this.min = min;
        this.max = max;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public static List<NodeStatus> getNodeStatusList() {
        List<NodeStatus> list = new ArrayList<>();

//        list.add(new NodeStatus("R","入场",10,30));
//        list.add(new NodeStatus("LX","落箱",10,30));
//        list.add(new NodeStatus("BTX","开始掏箱",10,30));
//        list.add(new NodeStatus("ETX","结束掏箱",10,30));
//        list.add(new NodeStatus("BCK","开始查验",10,30));
//        list.add(new NodeStatus("ECK","结束查验",10,30));
//        list.add(new NodeStatus("BZX","开始装箱",10,30));
//        list.add(new NodeStatus("EZX","装箱完成",10,30));
//        list.add(new NodeStatus("EXIT","出场",10,30));

        list.add(new NodeStatus("R","入场",5,15));
        list.add(new NodeStatus("LX","落箱",5,15));
        list.add(new NodeStatus("BTX","开始掏箱",15,25));
        list.add(new NodeStatus("ETX","结束掏箱",10,20));
        list.add(new NodeStatus("BCK","开始查验",10,20));
        list.add(new NodeStatus("ECK","结束查验",15,30));
        list.add(new NodeStatus("BZX","开始装箱",5,45));
        list.add(new NodeStatus("EZX","装箱完成",10,25));
        list.add(new NodeStatus("EXIT","出场",5,15));
        return list;
    }
}
