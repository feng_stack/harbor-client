package com.soyuan.model;

/**
 * Created by twx on 2017/10/25.
 */
public class NodeMongo {
    private boolean status;
    private NodeDTO data;

    public NodeMongo(boolean status, NodeDTO data) {
        this.status = status;
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public NodeDTO getData() {
        return data;
    }

    public void setData(NodeDTO data) {
        this.data = data;
    }
}
