package com.soyuan;

import com.soyuan.util.HttpClientUtil;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
            JobDetail job = JobBuilder.newJob(QuartzJob.class)
                    .withIdentity("twxJobName", "group1").build();

            Trigger trigger1 = TriggerBuilder.newTrigger()
                    .withIdentity("trigger1","group1")
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                            .withRepeatCount(0))
                    .build();

            //每天8点
            Trigger trigger2 = TriggerBuilder.newTrigger()
                    .withIdentity("trigger2", "group1")
                    .withSchedule(CronScheduleBuilder.cronSchedule("0 0 8 ? * *"))
                    .forJob(JobKey.jobKey("twxJobName","group1"))
                    .build();

            try {

                Scheduler scheduler = new StdSchedulerFactory().getScheduler();
                scheduler.start();
                scheduler.scheduleJob(job, trigger1);
                scheduler.scheduleJob(trigger2);
            } catch (SchedulerException e) {
                e.printStackTrace();
            }
        }
}
